#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: 8824088
#  First student's name: Scott Schultz
#  Portfolio contribution: 50%
#
#  Second student's no: #######
#  Second student's name: <name>
#  Portfolio contribution: 50%
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  EQUAL SHARES
#
#  In this task you will prove empirically that dividing a square
#  diagonally produces two equally-sized triangles.  To do so
#  you will need to use Turtle graphics and random numbers to conduct
#  an experiment in which you calculate the ratio of randomly-chosen
#  points appearing in each of the two triangles.  See the instructions
#  accompanying this file for full details. 
#
#--------------------------------------------------------------------#



# Import the necessary pre-defined functions
from turtle import *
from random import randint
from math import sqrt

#####  DEVELOP YOUR SOLUTION HERE

#################################

screenSize = 600
squareSize = 500
xPos, yPos = 0, 0
bottomLeft = xPos - squareSize/2, yPos - squareSize/2 # bottom left of square
diagonalLength = sqrt((squareSize ** 2) + (squareSize ** 2))
dotBorder = (screenSize/2)-10
numDots = 300

topHalfCount,bottomHalfCount = 0,0

#################################

def drawSquare(xPos, yPos, squareSize):
	penup()
	goto(bottomLeft) 
	fillcolor("purple") # lets pick one of the most garish colours we can
	begin_fill()
	setheading(0) # face east
	forward(squareSize) # bottom line
	left(90)
	forward(squareSize) # right-hand edge
	left(90)
	forward(squareSize) # top edge
	left(90)
	forward(squareSize) # left-hand edge
	end_fill()
	
def outlineSquare():
	penup()
	goto(bottomLeft) 
	color("black")
	pensize(width=2)
	pendown()
	setheading(0) # face east
	forward(squareSize) # bottom line
	left(90)
	forward(squareSize) # right-hand edge
	left(90)
	forward(squareSize) # top edge
	left(90)
	forward(squareSize) # left-hand edge

def makeTriangle():
	goto(bottomLeft)
	color("black")
	pensize(width=2)
	pendown()
	setheading(0)
	left(45)
	forward(diagonalLength)
	penup()
	hideturtle()
	#print bottomLeft

def drawDots():
	global topHalfCount, bottomHalfCount, outsideCount
	for _ in range(numDots):
		penup()
		randX = randint(-dotBorder, dotBorder)
		randY = randint(-dotBorder, dotBorder)
		goto(randX, randY)
		dotSize = 7

		# determain where the dots landed, if the y pos of the dot is greater than its x pos, but less than 250, its in the top half section
		if (randX < randY) and -250<randX<250 and randY<250:
			dotColor = 'red'
			topHalfCount += 1
		# likewise for here, if its x pos is greater than its y, and its y is greater than the floor of the box, its most likely in the bottom half
		elif (randX > randY) and -250<randX<250 and randY>-250:
			dotColor = 'blue'
			bottomHalfCount += 1

		# and if its outside, paint it black
		if randX > 250 or randX < -250 or randY > 250 or randY < -250:
			  dotColor = 'black'
		dot(dotSize,dotColor)
		
tracer(0)
setup(screenSize, screenSize)
title("Equal Shares")
bgcolor("green") # and then our complementary garish colour
penup()
speed("fastest")

drawSquare(xPos, yPos, squareSize)
outlineSquare()
makeTriangle()
drawDots()

print "Ratio of red:blue is %d:%d, from a total of %d dots tested." %(topHalfCount,bottomHalfCount,numDots)

done()
